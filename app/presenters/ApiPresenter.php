<?php

namespace App\Presenters;

use App\Model;
use Nette\Application\Responses\JsonResponse;

class ApiPresenter extends \App\Presenters\SecurePresenter
{

        /**
         * @var Model\Cars $cars
         * @inject
         */
        public $cars;
        
        public function renderCars()
        {
                $cars = $this->cars->findAll()->order('id');
                $json = array();
                foreach ($cars as $car) {
                        $json[] = $car->toArray();
                }
                $this->sendResponse(new JsonResponse($json));
        }

        public function renderCar($id)
        {
                $car = $this->cars->find($id)->toArray();
                $this->sendResponse(new JsonResponse($car));
        }

}
