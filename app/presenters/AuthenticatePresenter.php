<?php

namespace App\Presenters;

use \Firebase\JWT\JWT;
use Nette\Http\Response;

class AuthenticatePresenter extends \App\Presenters\BasePresenter
{

        /**
         * @var Response
         * @inject
         */
        public $httpResponse;
        
        private $user = array(
                "user" => "pavelslama",
                "email" => "user@email.cz",
                "password" => "ThisIsAPassword"
        );

        public function actionLogin()
        {
                $this->httpResponse->setCookie("user", JWT::encode(json_encode($this->user), $this->key), 0);
                $this->terminate();
        }
        
        public function actionLogout()
        {
                $this->httpResponse->deleteCookie("user");
                $this->terminate();
        }

}
