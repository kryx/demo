<?php

namespace App\Presenters;

use \Firebase\JWT\JWT;
use Nette\Http\Request;

class SecurePresenter extends \App\Presenters\BasePresenter
{
        /**
         * @var Request
         * @inject
         */
        public $httpRequest;

        public function beforeRender()
        {
                $user = $this->authenticate();

                if ($user == FALSE)
                {
                        throw new \Nette\Security\AuthenticationException();
                }
        }

        private function authenticate()
        {
                $cookies = $this->httpRequest;
                $user = $cookies->getCookie("user");

                if ($user == null)
                {
                        return FALSE;
                }
                                
                $decoded = json_decode(JWT::decode($user, $this->key, array('HS256')));

                if ($decoded == NULL)
                {
                        return false;
                }

                return $user;
        }

}
