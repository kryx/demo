<?php

namespace App\Presenters;

use Nette;
use Nette\DI\Container;

class BasePresenter extends Nette\Application\UI\Presenter
{

        /**
         * @var Container $container
         * @inject
         */
        public $container;
        
        protected $key;

        public function startup()
        {
                parent::startup();
                $params = $this->container->getParameters();
                $this->key = $params["JWT_key"];
        }

}
